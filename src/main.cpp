#include "setup.h"

void setup()
{
    Serial.begin(115200);
    delay(50);
    Serial.println("Serial OK!");

    EEPROM.begin(512);
    delay(50);
    Serial.println("EEPROM OK!");

    //Instancia objeto de jsonBuffer
    StaticJsonBuffer<512> jsonBuffer;

    /*============================================
    Escrita
    ============================================*/
    JsonObject &escrita = jsonBuffer.createObject();
    escrita["a"] = "aaaa";
    escrita["b"] = "bbbb";
    escrita["c"] = "cccc";
    escrita["d"] = "dddd";
    escrita["e"] = "eeee";

    escrita.printTo(Serial);

    String json;
    escrita.printTo(json);

    for (int i = 0; i < 512; ++i)
    {
      if (i <= json.length())
        EEPROM.write(i, json[i]);
      else
        EEPROM.write(i, 0);
    }
    EEPROM.commit();

    //Delay TENEBROSO
    delay(1000);

    /*============================================
    Leitura
    ============================================*/
    char character;
    String json_temp = "";
    for (int i = 0; i < 512; ++i)
    {
        character = char(EEPROM.read(i));
        if (character > 0)
        {
            json_temp += character;
        }
    }
    Serial.println(json_temp);

    JsonObject &leitura = jsonBuffer.parseObject(json_temp); //Converte json_temp em um obj JSON
    if (leitura.success())
    {
        String a = leitura["a"];
        Serial.println(a);
        String b = leitura["b"];
        Serial.println(b);
        String c = leitura["c"];
        Serial.println(c);
        String d = leitura["d"];
        Serial.println(d);
        String e = leitura["e"];
        Serial.println(e);
    }
}

void loop()
{
}